package ben.gentest;

import static ben.gentest.util.Const.CONTEXT_PATH;
import static ben.gentest.util.Const.PORT;
import static ben.gentest.util.Const.WEBAPP_DIRECTORY;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import org.apache.jasper.servlet.JspServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import ben.ladalja.DB;
import ben.ladalja.QueryListener;;

public class AppMain {

	private static final Logger LOGGER = LoggerFactory.getLogger(AppMain.class);
    
	public static void main(String[] args) throws Exception {
		
		DB.CONFIG_FILE_URL = new File("src/main/resources/database/gentest.properties")
								.toURI()
									.toURL()
										.toString();
		DB.disableTransaction();
		
		DB.register(new QueryListener(){

			public void listen(String query) {
				System.err.println(query);
			}
		
		});
		new AppMain();
		
	}
	
	
	
	
	
	private AppMain() throws Exception {
		LOGGER.debug("Starting server at port {}", PORT);
		Server server = new Server(PORT);
        
        server.setHandler(getWebAppContext(server));
		
        addRuntimeShutdownHook(server);
        System.out.println(new ClassPathResource(WEBAPP_DIRECTORY).getURI().toString());
        server.start();
        LOGGER.debug("Starting server at port {}", PORT);
        
        if(Desktop.isDesktopSupported()) {
        	//Desktop.getDesktop().browse(new URI("http://localhost:"+PORT));
        }else{
        	//Show message dialog
        }
        
        //System.getProperties().list(System.err);
        //server.join();
      
	}
	
	

    
	
    private static WebAppContext getWebAppContext(Server server) throws IOException {
    	
        WebAppContext context = new WebAppContext();
        context.setContextPath(CONTEXT_PATH);
        context.setResourceBase(new ClassPathResource(WEBAPP_DIRECTORY).getURI().toString());
        context.setClassLoader(Thread.currentThread().getContextClassLoader());
        context.addServlet(JspServlet.class, "*.jsp");
        
        context.setDescriptor("webapp/WEB-INF/web.xml");
        context.setAttribute("org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern",
                ".*/[^/]*servlet-api-[^/]*\\.jar$|.*/javax.servlet.jsp.jstl-.*\\.jar$|.*/.*taglibs.*\\.jar$");
        
        return context;
    }
    
    

    
    private static void addRuntimeShutdownHook(final Server server) {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            
            public void run() {
                if (server.isStarted()) {
                	server.setStopAtShutdown(true);
                    try {
                    	server.stop();
                    } catch (Exception e) {
                        System.out.println("Error while stopping jetty server: " + e.getMessage());
                        LOGGER.error("Error while stopping jetty server: " + e.getMessage(), e);
                    }
                }
            }
        }));
	}

}
