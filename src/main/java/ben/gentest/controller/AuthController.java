package ben.gentest.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import ben.gentest.model.Profile;
import ben.gentest.util.Const;
import ben.gentest.util.HashMan;

@Controller
public class AuthController {
	
	@RequestMapping(value = "/")
    public String index(Model m, HttpServletRequest request) {
		Profile profile = Profile.first(Profile.class);
		Profile.all(Profile.class);
		if(profile == null){
			String userName = System.getProperty("user.name");
			m.addAttribute("name", userName.substring(0, 1).toUpperCase() + userName.substring(1) ) ;
			return "register";
		}else{
			boolean isLogged = request.getSession().getAttribute(Const.ATTR_USER_SESSION) != null;
			if(isLogged || profile.getPassword() == null || profile.getPassword().isEmpty()){
				request.getSession().setAttribute(Const.ATTR_USER_SESSION, profile);
			}
			return "dashboard";	
		}
    }
	
	
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
    public String postRegister(Model m, 
    		@RequestParam(required=true) String name,
			@RequestParam(required=true) String grade,
			@RequestParam(required=false) String password,
			@RequestPart(required=false) MultipartFile pic,
			MultipartHttpServletRequest request) throws URISyntaxException, RuntimeException, SQLException 
	{
		Profile profile = new Profile();
		profile.setName(name);
		profile.setGrade(grade);
		
		if(password != null) {
			password = HashMan.hash(password);
		}
		
		profile.setPassword(password);
		
		//initAppHome();
	
		long time = System.currentTimeMillis();
		profile.setCreatedAt(time);
		profile.setUpdatedAt(time);
		
		profile.save();
		
		//Now handling image
		if(!pic.isEmpty()) {
			try {
				profile.updatePicture(pic.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return "redirect:/";
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(Model m, 
			@RequestParam(required=true) String password,
			HttpServletRequest request) 
	{
		Profile profile = Profile.first(Profile.class);
		boolean authenticate = HashMan.check(password, profile.getPassword());
		if( authenticate ){
			request.getSession().setAttribute(Const.ATTR_USER_SESSION, profile);
		}
		
		return new JSONObject().put("status", authenticate ).toString();
	}
	
	
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(Model m) {
		Runtime.getRuntime().exit(0);
    }
	
	@RequestMapping("/gentest")
	public String home() {
		return "redirect:/";
	}
	/*private void initAppHome() {
		 Create application home path 
		try{
			Path homePath = Paths.get(APP_HOME);
			if(!Files.exists(homePath)) {
				Files.createDirectories(Paths.get(APP_USER_PIC));
			}
			
			if(System.getProperty("os.name").toLowerCase().contains("windows")){//We are in windows system
				//set hidden attribute
		        Files.setAttribute(homePath, "dos:hidden", true, LinkOption.NOFOLLOW_LINKS);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}*/

}
