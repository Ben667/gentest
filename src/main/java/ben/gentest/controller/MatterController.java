/**
 * 
 */
package ben.gentest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ben.gentest.model.Matter;
import ben.gentest.util.Carbon;
import ben.ladalja.DB;

/**
 * @author Mezatsong
 *
 */

@Controller
@RequestMapping(value = "/matter")
public class MatterController {
	
	private static final String FOLDER = "matter/";
	
	@RequestMapping(value = "")
	public String index(Model m)
	{
		List<Matter> matters = Matter.all(Matter.class);
		Map<Integer,Long> controlCount = new HashMap<>();
		Map<Integer,String> dateC = new HashMap<>();
		Map<Integer,String> dateU = new HashMap<>();
		for(Matter matter : matters)
		{
			controlCount.put(matter.getId(), DB.table("controls").where("matter", matter.getName()).count() );
			dateC.put(matter.getId(), Carbon.toDateString(matter.getCreatedAt()));
			dateU.put(matter.getId(), Carbon.toDateString(matter.getUpdatedAt()));
		}
		m.addAttribute("matters", matters);
		m.addAttribute("dateC", dateC);
		m.addAttribute("dateU", dateU);
		m.addAttribute("controlCount", controlCount);
		return FOLDER+"index";
	}
	
	
	@RequestMapping(value="/show/{id}")
	public String show(Model m, @PathVariable Long id)
	{
		Matter matter = Matter.findOrFail(Matter.class, id);
		m.addAttribute("controlCount", DB.table("controls").where("matter", matter.getName()).count());
		m.addAttribute("matter", matter);
		return FOLDER+"show";
	}
	
	@RequestMapping("/create")
	public String create(Model m)
	{
		m.addAttribute("names", DB.table("matters").pluckList("name"));
		return FOLDER+"create";
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/store", method = RequestMethod.POST)
	public String store(Model m, @RequestParam(required=true) String name, HttpServletRequest request)
	{
		Long now = System.currentTimeMillis();
		Map<String,Object> data = new HashMap<>();
		data.put("name", name);
		data.put("created_at", now);
		data.put("updated_at", now);
		DB.table("matters").insert(data);
		//flash message:matter.flash.created
		return "redirect:/matter";
	}
	
	@RequestMapping("/edit/{id}")
	public String edit(Model m, @PathVariable int id)
	{
		m.addAttribute("matter", Matter.findOrFail(Matter.class, id));
		m.addAttribute("names", DB.table("matters").where("id", "<>", id).pluckList("name"));
		return FOLDER+"edit";
	}
	
	
	@RequestMapping(value="/update/{id}", method = RequestMethod.PUT)
	public String update(Model m, @RequestParam(required=true) String name, @PathVariable int id)
	{
		Matter matter = new Matter();
		matter.setId(id);
		matter.setName(name);
		matter.setUpdatedAt(System.currentTimeMillis());
		matter.save();
		//flash message:matter.flash.updated
		return "redirect:/matter";
	}
	
	@RequestMapping(value="/delete", method = RequestMethod.POST)
	public String delete(Model m, @RequestParam(required=true) Long id)
	{
		Matter.findOrFail(Matter.class,id).delete();
		//flash message:matter.flash.deleted
		return "redirect:/matter";
	}
}
