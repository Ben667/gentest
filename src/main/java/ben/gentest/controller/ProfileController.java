package ben.gentest.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ben.gentest.model.Profile;

@Controller
@RequestMapping(value = "/profile")
public class ProfileController {
	
	private static final String FOLDER = "profile/";
	
	@RequestMapping("")
	public String show(Model m)
	{
		m.addAttribute("profile", Profile.first(Profile.class));
		return FOLDER+"show-edit";
	}
	
	
	@RequestMapping(value = "/store", method = RequestMethod.POST)
	public String store(Model m, 
			@RequestParam(required=true) String name, 
			@RequestParam(required=true) String grade, 
			@RequestParam String password,  
			HttpServletRequest request)
	{
		//flash message:matter.flash.created
		return "redirect:/profile";
	}

}
