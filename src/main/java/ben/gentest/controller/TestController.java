package ben.gentest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/gentest")
public class TestController {

	@RequestMapping(value = "/home", method = RequestMethod.GET)
    public String test(Model m) {
		return "home";
    }
}
