/**
 * 
 */
package ben.gentest.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ben.gentest.model.Profile;
import ben.gentest.util.Const;

/**
 * @author Mezatsong
 *
 */
public class ProfileFilter implements Filter {

	public void init(FilterConfig arg0) throws ServletException {}
	
	public void destroy() {}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException 
	{
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		HttpSession session = request.getSession();
	
		if(session.getAttribute(Const.ATTR_USER_SESSION) == null) {
			Profile profile = Profile.first(Profile.class);
			if(profile != null && profile.getPassword() != null && !profile.getPassword().isEmpty())
			{
				request.setAttribute("profileName", profile.getGrade() +" "+profile.getName());
				request.setAttribute("profileImage", "/mdb_free/img/default-user-pic.png");
				request.setAttribute("isLogin", true);
			}
		}
		chain.doFilter(request,response);
	}

	

}
