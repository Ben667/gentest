/**
 * 
 */
package ben.gentest.model;

import ben.ladalja.Column;
import ben.ladalja.Model;

/**
 * @author Mezatsong
 *
 */
public class Control extends Model {

	private Long id;
	private String matter;
	private String name;
	private Integer rate;
	private String content;
	private String correction;
	@Column("school_level")private String schoolLevel;
	@Column("created_at")private Long createdAt;
	@Column("updated_at")private Long updatedAt;
	
	
	@Override
	protected String getTable() {
		return "controls";
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getMatter() {
		return matter;
	}


	public void setMatter(String matter) {
		this.matter = matter;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Integer getRate() {
		return rate;
	}


	public void setRate(Integer rate) {
		this.rate = rate;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getCorrection() {
		return correction;
	}


	public void setCorrection(String correction) {
		this.correction = correction;
	}


	public String getSchoolLevel() {
		return schoolLevel;
	}


	public void setSchoolLevel(String schoolLevel) {
		this.schoolLevel = schoolLevel;
	}


	public Long getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}


	public Long getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	
}
