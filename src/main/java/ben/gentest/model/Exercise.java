/**
 * 
 */
package ben.gentest.model;

import ben.ladalja.Column;
import ben.ladalja.Model;

/**
 * @author mezatsong
 *
 */
public class Exercise extends Model {

	
	private Long id;
	@Column("matter_id")private Long matterId;
	private String name;
	private Integer rate;
	private String source;
	private String content;
	private String correction;
	private Integer uses;
	@Column("school_level")private String schoolLevel;
	@Column("created_at")private Long createdAt;
	@Column("updated_at")private Long updatedAt;
	
	@Override
	protected String getTable() {
		return "exercises";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMatterId() {
		return matterId;
	}

	public void setMatterId(Long matterId) {
		this.matterId = matterId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCorrection() {
		return correction;
	}

	public void setCorrection(String correction) {
		this.correction = correction;
	}

	public String getSchoolLevel() {
		return schoolLevel;
	}

	public void setSchoolLevel(String schoolLevel) {
		this.schoolLevel = schoolLevel;
	}
	
	public Integer getUses() {
		return uses;
	}

	public void setUses(Integer uses) {
		this.uses = uses;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Matter getMatter() {
		return belongsTo(Matter.class, "matter_id");
	}
	
	public void setMatter(Matter matter) {
		associate(matter, "matter_id");
	}
}
