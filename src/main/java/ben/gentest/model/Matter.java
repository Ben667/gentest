package ben.gentest.model;

import java.util.List;

import ben.ladalja.Column;
import ben.ladalja.Model;

/**
 * 
 * @author Mezatsong
 *
 */
public class Matter extends Model {

	
	private Integer id;
	private String name;
	@Column("created_at")private Long createdAt;
	@Column("updated_at")private Long updatedAt;
	
	@Override
	protected String getTable() {
		return "matters";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<Exercise> getExercises() {
		return this.hasMany(Exercise.class, "matter_id");
	}
}
