/**
 * 
 */
package ben.gentest.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ben.ladalja.Column;
import ben.ladalja.DB;
import ben.ladalja.Ignore;
import ben.ladalja.Model;

/**
 * @author Mezatsong
 *
 */
public class Profile extends Model {

	
	private String name;
	private String password;
	private String grade;
	@Ignore private Byte pic[];
	@Column("created_at")private Long createdAt;
	@Column("updated_at")private Long updatedAt;
	
	@Override
	protected String getTable() {
		return "profile";
	}
	
	@Override
	protected String getPrimaryKey(){
		return "name";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}


	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}
	
    
    
    /**
     * Update picture for a specific material
     *
     * @param materialId
     * @param filename
     * @throws SQLException 
     */
    public void updatePicture(byte[] bytes) {
    	String updateSQL = "UPDATE profile SET pic = ? ";
    	Connection conn = DB.connection();
        try(PreparedStatement pstmt = conn.prepareStatement(updateSQL);) {
			pstmt.setBytes(1, bytes);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    
    /**
     * read the picture file and insert into the material master table
     *
     * @param materialId
     * @param filename
     */
    public void readPicture(String filename) {
        // update sql
        String selectSQL = "SELECT pic FROM profile LIMIT 1";
        ResultSet rs = null;
        FileOutputStream fos = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
 
        try {
            conn = DB.connection();
            pstmt = conn.prepareStatement(selectSQL);
            rs = pstmt.executeQuery();
 
            // write binary stream into file
            File file = new File(filename);
            fos = new FileOutputStream(file);
 
            while( rs.next() ) {
                InputStream input = rs.getBinaryStream("pic");
                if(input != null) {
	                byte[] buffer = new byte[1024];
	                while (input.read(buffer) > 0) {
	                    fos.write(buffer);
	                }
                }
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (fos != null) {
                    fos.close();
                }
 
            } catch (SQLException | IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
