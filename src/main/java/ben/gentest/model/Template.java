/**
 * 
 */
package ben.gentest.model;


import ben.ladalja.Column;
import ben.ladalja.Model;

/**
 * @author Mezatsong
 *
 */
public class Template extends Model {

	
	private Long id;
	private String header;
	private String footer;
	private String font;
	@Column("num_exo")private Integer numExo;
	@Column("num_part")private Integer numPart;
	private String content;
	private Integer uses;
	@Column("created_at")private Long createdAt;
	@Column("updated_at")private Long updatedAt;
	
	@Override
	protected String getTable() {
		return "templates";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public String getFont() {
		return font;
	}

	public void setFont(String font) {
		this.font = font;
	}

	public Integer getNumExo() {
		return numExo;
	}

	public void setNumExo(Integer numExo) {
		this.numExo = numExo;
	}

	public Integer getNumPart() {
		return numPart;
	}

	public void setNumPart(Integer numPart) {
		this.numPart = numPart;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public Integer getUses() {
		return uses;
	}

	public void setUses(Integer uses) {
		this.uses = uses;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	
}
