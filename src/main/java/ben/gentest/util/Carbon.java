package ben.gentest.util;

import java.sql.Date;
import java.sql.Time;

public final class Carbon {
	
	public static String toDateString(Long arg)
	{
		long time = System.currentTimeMillis();
		if(arg != null){
			time = arg;
		}
		return new Date(time).toString();
	}
	
	public static String toTimeString(Long arg)
	{
		long time = System.currentTimeMillis();
		if(arg != null){
			time = arg;
		}
		return new Time(time).toString();
	}
	
	public static String toDateTimeString(Long arg)
	{
		return toDateString(arg) + " " + toTimeString(arg);
	}
}
