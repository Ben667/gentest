/**
 * 
 */
package ben.gentest.util;

/**
 * @author Mezatsong
 *
 */
public final class Const {

	
	public static final int PORT = 8080;//9290;
	public static final String CONTEXT_PATH = "/";
	public static final String WEBAPP_DIRECTORY = "webapp";
	
	public static final String ATTR_USER_SESSION = "profile";

	public static final String APP_HOME = System.getProperty("user.home") + "/.GenTest";
	public static final String APP_PROFILE = APP_HOME + "/profile";
	public static final String APP_USER_PIC = APP_PROFILE + "/picture";
	
}
