package ben.gentest.util;

import org.jasypt.util.password.ConfigurablePasswordEncryptor;

/**
 * @author Mezatsong
 *
 */
public final class HashMan {
	
	private static final String ALGORITHM_LIST[] = {"MD5","SHA-224", "SHA-256", "SHA-384", "SHA-512"};
	private static final int CHOOSED_ALGO_INDEX = 0;
	private static final String ALGO = ALGORITHM_LIST[CHOOSED_ALGO_INDEX];
	
	
	private static ConfigurablePasswordEncryptor configure() {
		ConfigurablePasswordEncryptor encryptor = new ConfigurablePasswordEncryptor();
		encryptor.setAlgorithm(ALGO);
		return encryptor;
	}
	
	
	public static String hash(String password) {
		return configure().encryptPassword(password);
	}
	
	
	public static boolean check(String pwd, String cryptedPwd) {
		return configure().checkPassword(pwd, cryptedPwd);
	}
}
