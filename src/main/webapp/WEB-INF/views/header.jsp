<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>GenTest Application</title> 
    <style type="text/css">
	    @font-face {
	        font-family: 'FontAwesome';
	        src: url("<c:url value="/mdb_free/font/fontawesome-webfont.eot?v=4.7.0"/>");
	        src: url("<c:url value="/mdb_free/font/fontawesome-webfont.eot?#iefix&v=4.7.0"/>") format('embedded-opentype'), 
	            url("<c:url value="/mdb_free/font/fontawesome-webfont.woff2?v=4.7.0"/>") format('woff2'), 
	            url("<c:url value="/mdb_free/font/fontawesome-webfont.woff?v=4.7.0"/>") format('woff'), 
	            url("<c:url value="/mdb_free/font/fontawesome-webfont.ttf?v=4.7.0"/>") format('truetype'), 
	            url("<c:url value="/mdb_free/font/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular"/>") format('svg');
	        font-weight: normal;
	        font-style: normal;
	      }
    </style>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<c:url value="/mdb_free/css/font-awesome.min.css"/>">
    <!-- Bootstrap core CSS -->
    <link href="<c:url value="/mdb_free/css/bootstrap.min.css"/>" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<c:url value="/mdb_free/css/mdb.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/mdb_free/css/compiled-4.5.9.min.css"/>" rel="stylesheet">
    <!-- My custom styles (optional) -->
    <link href="<c:url value="/mdb_free/css/style.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/mdb_free/css/custom.css"/>" rel="stylesheet">
    
    <!-- Favicon -->
    <link href="<c:url value="/mdb_free/img/logo/white_gentest.png"/>" rel="icon" type="image/png" />

</head>

<body class="grey lighten-3">

    <!--Main Navigation-->
    <header>

        <!-- Navbar -->
        <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
            <div class="container-fluid">

                <!-- Brand -->
                <a class="navbar-brand waves-effect" href="#" >
                    <strong class="blue-text">MDB</strong>
                </a>

                <!-- Collapse -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Links -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <!-- Left -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link waves-effect" href="#">Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="#">About MDB</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="#">Free download</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="#">Free tutorials</a>
                        </li>
                    </ul>

                    <!-- Right -->
                    <ul class="navbar-nav nav-flex-icons">
                        <li class="nav-item">
                            <a href="https://www.facebook.com/" class="nav-link waves-effect" target="_blank" >
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="https://twitter.com/Mezatsong" class="nav-link waves-effect" target="_blank" >
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="https://github.com/Mezatsong" class="nav-link border border-light rounded waves-effect" target="_blank">
                                <i class="fa fa-github mr-2"></i>Mezatsong GitHub
                            </a>
                        </li>
                    </ul>

                </div>

            </div>
        </nav>
        <!-- Navbar -->

        <!-- Sidebar -->
        <div class="sidebar-fixed position-fixed">

            <a class="logo-wrapper waves-effect">
                <img src="<c:url value="/mdb_free/img/logo/gentest.png"/>" class="img-fluid" alt="" width="142" >
            </a>

            <div class="list-group list-group-flush">
                <a href="<c:url value="/"/>" class="list-group-item list-group-item-action waves-effect ${ param.page == 'Dashboard' ? 'active':'' }">
                    <i class="fa fa-pie-chart mr-3"></i><spring:message code="menu.dashboard"/>
                </a>
                <a href="<c:url value="/profile"/>" class="list-group-item list-group-item-action waves-effect ${ param.page == 'Profile' ? 'active':'' }">
                    <i class="fa fa-user mr-3"></i><spring:message code="menu.profil"/>
               	</a>
                <a href="<c:url value="/matter"/>" class="list-group-item list-group-item-action waves-effect ${ param.page == 'Matter' ? 'active':'' }">
                    <i class="fa fa-file-pdf-o mr-3"></i><spring:message code="menu.matter"/>
                </a>
                <a href="#" class="list-group-item list-group-item-action waves-effect ${ param.page == 'Maps' ? 'active':'' }">
                    <i class="fa fa-paint-brush mr-3"></i><spring:message code="menu.template"/>
                </a>
                <a href="#" class="list-group-item list-group-item-action waves-effect ${ param.page == 'Orders' ? 'active':'' }">
                    <i class="fa fa-edit mr-3"></i><spring:message code="menu.control"/>
                </a>
                <a href="#" class="list-group-item list-group-item-action waves-effect ${ param.page == 'Maps' ? 'active':'' }">
                    <i class="fa fa-gear mr-3"></i><spring:message code="menu.setting"/>
                </a>
                <button class="list-group-item list-group-item-action waves-effect ${ param.page == 'Orders' ? 'active':'' }" data-toggle="modal" data-target="#ExitModal">
                    <i class="fa fa-sign-out mr-3"></i><spring:message code="menu.exit"/>
                </button>
            </div>

        </div>
        <!-- Sidebar -->

    </header>
    <!--Main Navigation-->
	
	<!-- notifications -->
	<c:import url="/WEB-INF/views/notification.jsp" />
	
    <!--Main layout-->
    <main class="pt-5 mx-lg-5">