<c:import url="../header.jsp">
	<c:param name="title" value="Create matter" />
	<c:param name="page" value="Matter" />
</c:import>


<div class="container-fluid mt-5">

    <!-- Heading -->
    <div class="card mb-4 wow fadeIn">

        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">

            <h4 class="mb-2 mb-sm-0 pt-1">
                <a href="<c:url value="/matter"/>"><spring:message code="menu.matter" /></a>
                <span>/</span>
                <span><spring:message code="matter.title.create"/></span>
            </h4>

        </div>

    </div>
    <!-- Heading -->
    
    
    <!--Grid column-->
    <div class="col-md-12 my-4 py-5">

       	<!--Form -->
        <form action="<c:url value="/matter/store"/>" method="post" >   
            <div class="card">
                <div class="card-body">

                    <!--Body-->
                    <div class="md-form">
                        <i class="fa fa-edit prefix blue-text"></i>
                        <input type="text" id="name" class="form-control" name="name" autocomplete="off" required value="">
                        <label for="name" class=""><spring:message code="matter.createform.label.name"/></label>
                    </div>
                    <p class="red-text" id="error" style="display:none;"><spring:message code="matter.createform.error.name"/></p>
                    <div class="text-center">
                        <button class="btn btn-primary waves-effect waves-light">
                        	<span style="text-transform: upper-case"><spring:message code="matter.createform.button.create"/></span>
                         </button>
                     </div>
                </div> 
            </div>
       	</form>
  		<!--/Form -->

     </div>
                
    <!--Grid column-->
    
</div>
    
<c:import url="../footer.jsp"/>


<script>
	$('form').submit(function(e){
		var name = $('#name').val();
		var alreadyThere = false;
		<c:forEach items="${ names }" var="item">
			if(name == '${ item }'){
				alreadyThere = true;
			}
		</c:forEach>
		if(alreadyThere){
			e.preventDefault();
			$('#error').show(450);
		}
	});
	
	$('#name').keyup(function(){
		var name = $('#name').val();
		var alreadyThere = false;
		<c:forEach items="${ names }" var="item">
			if(name == '${ item }'){
				alreadyThere = true;
			}
		</c:forEach>
		if(alreadyThere){
			$('#error').show(150);
		}else{
			$('#error').hide(150);
		}
	});
	
</script>

