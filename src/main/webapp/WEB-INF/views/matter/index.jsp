<c:import url="../header.jsp">
	<c:param name="page" value="Matter" />
</c:import>


<div class="container-fluid mt-5">

    <!-- Heading -->
    <div class="card mb-4 wow fadeIn">

        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">

            <h4 class="mb-2 mb-sm-0 pt-1"><!-- 
                <a href="#" target="_blank">Home Page</a>
                <span>/</span> -->
                <span><spring:message code="menu.matter"/></span>
            </h4>
			<c:if test="${ matters.size() > 0 }">
				<div class="d-flex justify-content-center">
	                <a href="<c:url value="/matter/create" />" class="btn btn-primary btn-sm my-0 p">
	                    <i class="fa fa-plus"></i> <spring:message code="matter.button.create"/>
	                </a>
	            </div>
            </c:if>
        </div>

    </div>
    <!-- Heading -->
    
    
    <!--Grid column-->
    <div class="col-md-12 mb-4">

        <!--Card-->
        <div class="card">

            <!--Card content-->
            <div class="card-body">
				<c:choose>
					<c:when test="${ matters.size() < 1 }">
						<div class="row my-5 py-5">
							<div class="col-md-10 text-center">
								<h1><spring:message code="matter.list.empty" /></h1>
								<a href="<c:url value="/matter/create" />" class="btn btn-primary">
									<spring:message code="matter.button.create"/>
								</a>
							</div>
						</div>
					</c:when>
					<c:otherwise>
		                <!-- Table  -->
		                <img alt="Ben a raison" src="file:///home/mezatsong/Pictures/4.png" />
		                <table id="${matters.size() > 10 ? 'datatable':'table' }" class="table table-hover ${ matters.size() < 5 ? 'my-5 py-5' : ''}">
		                    <!-- Table head -->
		                    <thead class="blue-grey lighten-4">
		                        <tr>
		                            <th><spring:message code="matter.list.label.name" /></th>
		                            <th><spring:message code="matter.list.label.controls" /></th>
		                            <th><spring:message code="matter.list.label.exercises" /></th>
		                            <th><spring:message code="matter.list.label.created_at" /></th>
		                            <th><spring:message code="matter.list.label.updated_at" /></th>
		                            <th class="text-center"><spring:message code="matter.list.label.action" /></th>
		                        </tr>
		                    </thead>
		                    <!-- Table head -->
		
		                    <!-- Table body -->
		                    <tbody>
		                    	<c:forEach items="${ matters }" var="matter">
			                        <tr>
			                            <th scope="row" id="name<c:out value="${ matter.getId() }" />"><c:out value="${ matter.getName() }" /></th>
			                            <td><c:out value="${ controlCount.get( matter.getId() ) }" /></td>
			                            <td id="exo<c:out value="${ matter.getId() }" />"> <c:out value="${ matter.getExercises().size() }" /></td>
			                            <td><c:out value="${ dateC.get(matter.getId()) }" /></td>
			                            <td><c:out value="${ dateU.get(matter.getId()) }" /></td>
			                            <td>
			                            	<span class="badge badge-primary badge-pill pull-right">
				                                <a href="/matter/edit/<c:out value="${ matter.getId() }"/>">
				                                	<i class="fa fa-edit ml-1"></i>Edit
				                                </a>
				                            </span>
			                            	<span class="badge badge-success badge-pill pull-right">
				                                <a href="/matter/show/<c:out value="${ matter.getId() }"/>">
				                                	<i class="fa fa-eye ml-1"></i>Show
				                                </a>
				                            </span>
			                            	<span class="badge badge-danger badge-pill pull-right">
				                                <a href="<c:out value="${ matter.getId() }"/>" class="delete-but" data-toggle="modal" data-target="#deleteModal" >
				                                	<i class="fa fa-remove ml-1"></i>Remove
				                                </a>
				                            </span>
			                            </td>
			                        </tr>
		                        </c:forEach>
		                    </tbody>
		                    <!-- Table body -->
		                </table>
		                <!-- Table  -->
		        	</c:otherwise>
				</c:choose>
            </div>

        </div>
        <!--/.Card-->

    </div>
    <!--Grid column-->
    
</div>


 <!-- Side Modal Top Left Info-->
 <div class="modal fade left" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     data-backdrop="false">
     <div class="modal-dialog modal-side modal-top-left modal-notify modal-info" role="document">
         <!--Content-->
         <div class="modal-content">
             <!--Header-->
             <div class="modal-header">
                 <p class="heading lead"><spring:message code="button.text.delete"/> <strong id="del-name"></strong></p>

                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true" class="white-text">&times;</span>
                 </button>
             </div>

             <!--Body-->
             <div class="modal-body">

                 <div class="text-center">
                     <p>
                     	<span id="message"></span>
                     </p>
                 </div>
             </div>

             <!--Footer-->
             <div class="modal-footer justify-content-center">
             <form action="<c:url value="/matter/delete"/>" id="del-form" method="post">
             	 <input type="hidden" value="" name="id" id="del-id"/>
                 <button id="send" type="submit" role="button" class="btn btn-info"> <spring:message code="button.text.yes" />
                     <i class="fa fa-diamond ml-1"></i>
                 </button>
                 <a role="button" id="cancel" class="btn btn-outline-info waves-effect" data-dismiss="modal"><spring:message code="button.text.no" /></a>
             </form>
             </div>
         </div>
         <!--/.Content-->
     </div>
 </div>
 <!-- Side Modal Top Left Info-->


    
<c:import url="../footer.jsp"/>

<script type="text/javascript">
	$('.delete-but').click(function(){
		var id = $(this).attr('href');
		var exoCount = $('#exo'+id).html();
		var name = $('#name'+id).html();
		$('#del-name').html(name);
		if(exoCount > 0){
			$('#message').html('<spring:message code="matter.deleteform.candeleteitem" />');
			$('#send').hide();
			$('#cancel').html('<spring:message code="button.text.ok" />');
		}else{
			$('#message').html('<spring:message code="matter.deleteform.deleteitem" />');
			$('#del-id').val(id);
			$('#send').show();
			$('#cancel').html('<spring:message code="button.text.no" />');
		}
	});
	
</script>

