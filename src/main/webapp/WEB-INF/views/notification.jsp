
<c:if test="${! empty error }">
	<div class="alert alert-danger alert-dismissable margin5">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    <strong>Error:</strong> ${ error }
	</div>
</c:if>

<c:if test="${! empty success }">
	<div class="alert alert-success alert-dismissable margin5">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    <strong>Success:</strong> ${success}
	</div>
</c:if>

<c:if test="${! empty warning }">
	<div class="alert alert-warning alert-dismissable margin5">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    <strong>Warning:</strong> ${warning}
	</div>
</c:if>

<c:if test="${! empty info }">
	<div class="alert alert-info alert-dismissable margin5">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    <strong>Info:</strong> ${info}
	</div>
</c:if>