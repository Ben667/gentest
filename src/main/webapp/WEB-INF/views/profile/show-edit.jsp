<c:import url="../header.jsp">
	<c:param name="title" value="Profile" />
	<c:param name="page" value="Profile" />
</c:import>


<div class="container-fluid mt-5">

    <!-- Heading -->
    <div class="card mb-4 wow fadeIn">

        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">

            <h4 class="mb-2 mb-sm-0 pt-1">
               View and update your profile
            </h4>

        </div>

    </div>
    <!-- Heading -->
    
    
    <!--Grid column-->
    <div class="col-md-12 my-4 pb-4 pt-0">

       	<!--Form -->
        <form action="<c:url value="/profile/update"/>" method="post" >   
            <div class="card">
                <div class="card-body">

                    <!--Body-->
                    <div class="md-form">
                        <i class="fa fa-user prefix blue-text"></i>
                        <input type="text" id="name" class="form-control" name="name" autocomplete="off" required value="${ profile.name }">
                        <label for="name" class="">Name</label>
                        <span class="red-text">:message</span>
                    </div>
                    
                    <div class="md-form">
                        <i class="fa fa-graduation-cap prefix blue-text"></i>
                        <input type="text" id="grade" class="form-control" name="grade" autocomplete="off" required value="${ profile.grade }">
                        <label for="grade" class="">Grade</label>
                        <span class="red-text">:message</span>
                    </div>
                    
                    <p><i>Leave these fields empty if you don't want to change your password</i></p>
                    
                    <div class="md-form">
                        <i class="fa fa-lock prefix blue-text"></i>
                        <input type="password" id="password" class="form-control" name="password" autocomplete="off" value="">
                        <label for="name" class="">Password</label>
                        <span class="red-text">:message</span>
                    </div>
                    
                    
                    <div class="md-form">
                        <i class="fa fa-lock prefix blue-text"></i>
                        <input type="password" id="password_confirm" class="form-control" name="password_confirm" autocomplete="off" required value="">
                        <label for="name" class="">Password Confirm</label>
                        <span class="red-text">:message</span>
                    </div>
                    
                    <div class="text-center">
                        <button class="btn btn-primary waves-effect waves-light">
                        	<span style="text-transform: upper-case"><spring:message code="button.text.update"/></span>
                         </button>
                     </div>
                </div> 
            </div>
       	</form>
  		<!--/Form -->

     </div>
                
    <!--Grid column-->
    
</div>
    
<c:import url="../footer.jsp"/>

<script type="text/javascript" src="<c:url value="/mdb_free/js/compiled-4.5.9.min.js"/>"></script>
<script>
	var grades = [
	    "Mr",
	    "Mme",
	    "PLEG",
	    "Dr",
	    "Pr"
	];
	
	$('#grade').mdb_autocomplete({
	    data: grades
	});
	
	function passwordChanged(){
   		var cp = $('#password_confirm');
   		cp.val('');
   		if( $('#password').val() ){
   			cp.removeAttr('disabled');
   		}else{
   			cp.attr('disabled','disabled');
   		}
    }
    
    $('#password').change(passwordChanged);
    
    
    $('form').submit(function(e){
    	if( $('#password').val() != $('#password_confirm').val() ){
    		alert('<spring:message code="register.validation.password" />');
    		e.preventDefault();
    	}
    }); 
    
    
    $(document).ready(function(){
    	passwordChanged();
    	$('span.red-text').hide();
    });
	
</script>

