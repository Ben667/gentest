
<!DOCTYPE html>
<html>

	<head>
	    <meta charset="UTF-8">
	    <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> -->
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta http-equiv="x-ua-compatible" content="ie=edge">
	    <title><spring:message code="register.title"/> | GenTest</title> 
	    <style type="text/css">
		    @font-face {
		        font-family: 'FontAwesome';
		        src: url("<c:url value="/mdb_free/font/fontawesome-webfont.eot?v=4.7.0"/>");
		        src: url("<c:url value="/mdb_free/font/fontawesome-webfont.eot?#iefix&v=4.7.0"/>") format('embedded-opentype'), 
		            url("<c:url value="/mdb_free/font/fontawesome-webfont.woff2?v=4.7.0"/>") format('woff2'), 
		            url("<c:url value="/mdb_free/font/fontawesome-webfont.woff?v=4.7.0"/>") format('woff'), 
		            url("<c:url value="/mdb_free/font/fontawesome-webfont.ttf?v=4.7.0"/>") format('truetype'), 
		            url("<c:url value="/mdb_free/font/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular"/>") format('svg');
		        font-weight: normal;
		        font-style: normal;
		      }
	    </style>
	    <!-- Font Awesome -->
	    <link rel="stylesheet" href="<c:url value="/mdb_free/css/font-awesome.min.css"/>">
	    <!-- Bootstrap core CSS -->
	    <link href="<c:url value="/mdb_free/css/bootstrap.min.css"/>" rel="stylesheet">
	    <!-- Material Design Bootstrap -->
	    <link href="<c:url value="/mdb_free/css/mdb.min.css"/>" rel="stylesheet">
	    <link href="<c:url value="/mdb_free/css/compiled-4.5.9.min.css"/>" rel="stylesheet">
	    <!-- My custom styles (optional) -->
	    <link href="<c:url value="/mdb_free/css/style2.min.css"/>" rel="stylesheet">
	    <link href="<c:url value="/mdb_free/css/custom.css"/>" rel="stylesheet">
	    
	    <!-- Favicon -->
	    <link href="<c:url value="/mdb_free/img/logo/white_gentest.png"/>" rel="icon" type="image/png" />
	
	</head>
	<body class="grey lighten-3" style="background-image: url('<c:url value="/mdb_free/img/sample.jpg"/>'); background-repeat: no-repeat;background-size: cover; "> 

		<div class="row p-5">

			<div class="col-md-6 mb-4 mt-5 offset-3">

               	<!--Form with header-->
                <form action="<c:url value="/register"/>" method="post" enctype="multipart/form-data">   
                    <div class="card">
                        <div class="card-body">

                            <!--Header-->
                            <div class="form-header purple-gradient">
                                <h3>
                                    <i class="fa fa-smile-o"></i> <spring:message code="register.header"/> </h3>
                            </div>

                            <!--Body-->
                            <div class="md-form">
                                <i class="fa fa-user prefix grey-text"></i>
                                <input type="text" id="name" class="form-control" name="name" autocomplete="off" required value="${ name }">
                                <label for="name" class=""><spring:message code="register.label.name"/> <small>*</small></label>
                            </div>
                            
                            <div class="md-form">
                                <i class="fa fa-graduation-cap prefix grey-text"></i>
                                <input type="text" id="grade" class="form-control" name="grade" autocomplete="off" required value="Mr">
                                <label for="grade" class=""><spring:message code="register.label.grade"/> <small>*</small></label>
                            </div>

                            <div class="md-form">
                                <i class="fa fa-unlock prefix grey-text"></i>
                                <input type="password" id="password" class="form-control" name="password" autocomplete="off" value="">
                                <label for="password" class="" ><spring:message code="register.label.password"/></label>
                            </div>
                            
                            <div class="md-form">
                                <i class="fa fa-lock prefix grey-text"></i>
                                <input type="password" id="password_confirm" class="form-control" name="password_confirm" autocomplete="off" required>
                                <label for="password_confirm" class="" ><spring:message code="register.label.password_confirm"/> <small id="small_cp" style="display:none;"> *</small></label>
                            </div>
                            
                            <div class="file-field">
						        <div id="fa-pic-parent" class="btn-floating btn-sm btn-blue-grey my-0 waves-effect waves-light float-left">
						            <i id="fa-pic" class="fa fa-picture-o"></i>
						            <input type="file" name="pic" id="pic">
						        </div>
						        <div class="file-path-wrapper md-form">
						            <input id="file-path" class="file-path form-control validate" type="text" placeholder="<spring:message code="register.label.picture"/>">
						        </div>
						    </div>
                            <div class="text-center">
                                <button class="btn btn-primary purple-gradient waves-effect waves-light">
                                	<span style="text-transform: upper-case"><spring:message code="register.button"/></span>
                                </button>
                            </div>
							<p><small class="grey-text"><spring:message code="register.required" /></small></p>
                        </div> 
                    </div>
               	</form>
          		<!--/Form with header-->

                </div>
                
                
        </div>   
               
                
			
	    <!-- SCRIPTS -->
	    <!-- JQuery -->
	    <script type="text/javascript" src="<c:url value="/mdb_free/js/jquery-3.3.1.min.js"/>"></script>
	    <!-- Bootstrap tooltips -->
	    <script type="text/javascript" src="<c:url value="/mdb_free/js/popper.min.js"/>"></script>
	    <!-- Bootstrap core JavaScript -->
	    <script type="text/javascript" src="<c:url value="/mdb_free/js/bootstrap.min.js"/>"></script>
	    <!-- MDB core JavaScript -->
	    <script type="text/javascript" src="<c:url value="/mdb_free/js/mdb.min.js"/>"></script>
	    
	    <!-- Initializations -->
	    <script type="text/javascript">
	        // Animations initialization
	        new WOW().init();
	    </script>
	    <!-- MDB Select form initialization -->
	    <script type="text/javascript" src="<c:url value="/mdb_free/js/compiled-4.5.9.min.js"/>"></script> 
	    <script type="text/javascript">
	    	$(document).ready(function() {
		    	$('.mdb-select').material_select();
		   	});
		</script>
	    <!-- Custom -->
	    <script>
	    	
		    var grades = [
		        "Mr",
		        "Mme",
		        "PLEG",
		        "Dr",
		        "Pr"
		    ];
	
		    $('#grade').mdb_autocomplete({
		        data: grades
		    });
	    	
		    
		    //file-path pic
		    $('#pic').change(function(){
		    	$('#file-path').val( $(this).val() );
		    });
		    
		    function passwordChanged(){
		   		var cp = $('#password_confirm');
		   		cp.val('');
		   		if( $('#password').val() ){
		   			cp.removeAttr('disabled');
		   		}else{
		   			cp.attr('disabled','disabled');
		   			smallCp.hide();
		   		}
		    }
		    
		    $('#password').change(passwordChanged);
		    
		    
		    $('#password').keyup(function(){
		   		var smallCp = $('#small_cp');
		   		if( $(this).val() ){
		   			smallCp.show();
		   		}else{
		   			smallCp.hide();
		   		}
		    });
		    
		    
		    $('form').submit(function(e){
		    	if( $('#password').val() != $('#password_confirm').val() ){
		    		alert('<spring:message code="register.validation.password" />');
		    		e.preventDefault();
		    	}
		    });
		    
		    
		    $(document).ready(function(){
		    	passwordChanged();
		    });
		    
		    
		    
        </script>

</body>

</html>